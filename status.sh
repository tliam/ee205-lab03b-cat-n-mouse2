#!/bin/bash

echo I am
whoami
echo The current working directory is
pwd
echo The system I am on is
uname -n
echo The linux version is
uname -r
echo The linux distribution is
cat /etc/redhat-release
echo The system has been up for
uptime
echo The amount of disk space I\'m using in KB is
du -k ~ > kbData
tail -1 kbData
