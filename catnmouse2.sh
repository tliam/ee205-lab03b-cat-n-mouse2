#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))
MIN_VALUE=0
echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF
#this is how you write a comment in a bash script
echo Okay cat, I am thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:
guess=0
while (( $guess != $THE_NUMBER_IM_THINKING_OF ))
do
   read -r guess
   if (($guess <= $MIN_VALUE))
   then 
      echo You must enter a number that\'s \>\= 1
      continue
   fi

   if (($guess > $THE_MAX_VALUE))
   then
      echo You must enter a number that\'s \<\= $THE_MAX_VALUE
      continue
   fi

   if (($guess > $THE_NUMBER_IM_THINKING_OF))
   then
      echo No cat... the number I\'m thinking of is smaller than $guess
      continue
   fi

   if (($guess < $THE_NUMBER_IM_THINKING_OF))
   then
      echo No cat... the number I\'m thinking of is larger than $guess
      continue
   fi
done


echo  \ \ \ \ \ \|\\__/,\|   \(\`\\
echo  \ \  _.\|o o  \|_   \) \)
echo -\(\(\(---\(\(\(--------
#read -r is a scanf() equivalent for bash scripts
#echo $guess

